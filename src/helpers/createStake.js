import { getTokenBalance } from "./tokenBalance";
import { createTokenContract, getAllowance } from "./commonOperations";
import { markRaw } from "vue";

export const createStake = async (signer, account, tokensConfig) => {
  const mainTokenContract = await createTokenContract(
    tokensConfig.mainToken.address,
    tokensConfig.mainToken.abi,
    signer
  );
  const mainTokenBalance = await getTokenBalance(
    account,
    mainTokenContract,
    tokensConfig.mainToken.decimals
  );

  const stakeTokenContract = await createTokenContract(
    tokensConfig.stakeToken.address,
    tokensConfig.stakeToken.abi,
    signer
  );

  const stakeTokenBalance = await await getTokenBalance(
    account,
    stakeTokenContract,
    tokensConfig.stakeToken.decimals
  );

  const allowance = await getAllowance(
    account,
    tokensConfig.stakeToken.address,
    mainTokenContract
  );

  return markRaw({
    mainToken: {
      name: tokensConfig.mainToken.name,
      icon: tokensConfig.mainToken.icon,
      contract: mainTokenContract,
      balance: mainTokenBalance,
      adress: tokensConfig.mainToken.address,
      price: null,
    },
    stakeToken: {
      name: tokensConfig.stakeToken.name,
      icon: tokensConfig.stakeToken.icon,
      contract: stakeTokenContract,
      balance: stakeTokenBalance,
      adress: tokensConfig.stakeToken.address,
      price: null,
      allowance: allowance,
    },
    tokenRate: null,
  });
};
