// import store from "@/store";

export const switchNetwork = async (data) => {
    try {
        await this.provider.provider.request({
            method: "wallet_switchEthereumChain",
            params: [
                {
                    chainId: data.chainId === "0x01" ? "0x1" : data.chainId,
                },
            ],
        });

        if (this.isCoinbase) {
            window.location.reload();
        }
    } catch (error) {
        console.log("switch network err:", error);
        try {
            await this.provider.provider.request({
                method: "wallet_addEthereumChain",
                params: [data],
            });

            if (this.isCoinbase) {
                window.location.reload();
            }
        } catch (e) {
            console.log("wallet_addEthereumChain err:", e);
        }
    }
}

export const switchNetworkWithoutConnect = async (chainId) => {
    localStorage.setItem("CHAIN_ID", JSON.stringify(chainId));
    window.location.reload();
}
