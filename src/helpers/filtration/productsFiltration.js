import store from "@/store";

export const filterProducts = (products = []) => {
  let filteredProducts
  if (products.length > 0) {
    filteredProducts = products
  } else {
    filteredProducts = store.getters.getProducts;
  }

  const chosenCategories = store.getters.getChosenCategories;
  const filters = store.getters.getAttachedFilters;

  //sort by category
  if (!chosenCategories.includes("all")) {
    filteredProducts = filteredProducts.filter((product) =>
      chosenCategories.includes(product.category)
    );
  }

  //sort by brand
  if (filters.brand && filters.brand !== "All") {
    filteredProducts = filteredProducts.filter(
      (product) => product.brand === filters.brand
    );
  }
  //sort by price
  switch (filters.price) {
    case "Highest price":
      filteredProducts.sort((a, b) => (a.price > b.price ? -1 : 1));
      break;
    case "Lowest price":
      filteredProducts.sort((a, b) => (a.price > b.price ? 1 : -1));
      break;
    default: break;
  }

  if (filters.priceRange) {
    filteredProducts = filteredProducts.filter(product => +product.price <= +filters.priceRange)
  }

  //sort by rating
  switch (filters.rating) {
    case "Top rating":
      filteredProducts.sort((a, b) => (a.rating > b.rating ? -1 : 1));
      break;
    case "Low rating":
      filteredProducts.sort((a, b) => (a.rating > b.rating ? 1 : -1));
      break;
    default: break;
  }

  store.dispatch("fetchFilteredProducts", filteredProducts);
  return filteredProducts
};
