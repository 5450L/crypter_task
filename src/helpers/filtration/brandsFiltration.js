import store from "@/store"

export const sortBrands = () => {
    const sortedBrands = ['All']
    const chosenCategories = store.getters.getChosenCategories
    const allProducts = store.getters.getProducts

    if (!chosenCategories.includes('all')) {
        allProducts.map(item => {
            if (chosenCategories.includes(item.category) && !sortedBrands.includes(item.brand)) {
                sortedBrands.push(item.brand)
            }
        })

    } else {
        allProducts.map(item => {
            if (!sortedBrands.includes(item.brand)) {
                sortedBrands.push(item.brand);
            }
        })
    }

    store.dispatch('fetchSortedBrands', sortedBrands);
}