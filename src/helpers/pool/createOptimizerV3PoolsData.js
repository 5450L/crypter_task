import poolsInfo from "@/utils/config/pools";
import store from "@/store";
import { markRaw } from "vue";
import { ethers } from "ethers";
import { getTokenPriceByAddress } from "@/helpers/tokenPrice";

const createOptimizerV3PoolsData = async (chainId, signer) => {
  if (!poolsInfo || !chainId) return false;
  const chainPools = poolsInfo[chainId];

  let pools = [];

  chainPools.map(async (poolInfo) => {
    pools.push(markRaw(await createPool(poolInfo, signer)));
  });

  store.commit("setOptimizerPoolsInfo", pools);
};

//

const createPool = async (poolInfo, signer) => {
  const poolContract = await new ethers.Contract(
    poolInfo.poolContract.adress,
    JSON.stringify(poolInfo.poolContract.abi),
    signer
  );

  const token0Contract = await new ethers.Contract(
    poolInfo.token0.adress,
    JSON.stringify(poolInfo.token0.abi),
    signer
  );
  const token1Contract = await new ethers.Contract(
    poolInfo.token1.adress,
    JSON.stringify(poolInfo.token1.abi),
    signer
  );
  const token0Price = await getTokenPriceByAddress(
    poolInfo.chainId,
    poolInfo.token0.adress
  );
  const token1Price = await getTokenPriceByAddress(
    poolInfo.chainId,
    poolInfo.token1.adress
  );

  const token0TotalSupply = await token0Contract.totalSupply();
  const token1TotalSupply = await token1Contract.totalSupply();

  return {
    poolId: poolInfo.poolId,
    isDeprecated: poolInfo.isDeprecated,
    lpIcon: poolInfo.lpIcon,
    poolContract: poolContract,
    token0: {
      symbol: poolInfo.token0.symbol,
      icon: poolInfo.token0.icon,
      canBeDefault: poolInfo.token0.canBeDefault,
      decimals: poolInfo.token0.decimals,
      totalSupply: token0TotalSupply,
      price: token0Price,
    },
    token1: {
      symbol: poolInfo.token1.symbol,
      icon: poolInfo.token1.icon,
      canBeDefault: poolInfo.token1.canBeDefault,
      decimals: poolInfo.token1.decimals,
      totalSupply: token1TotalSupply,
      price: token1Price,
    },
  };
};

export { createOptimizerV3PoolsData };
