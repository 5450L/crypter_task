import poolsInfo from "@/utils/config/pools";
import store from "@/store";

export const createOptimizerV3CardsData = async (chainId) => {
  if (!poolsInfo || !chainId) return false;

  const chainPools = poolsInfo[chainId];
  const poolCardsInfo = [];
  chainPools.map((pool) => poolCardsInfo.push(createOptimizerCard(pool)));
  store.commit("setOptimizerPoolCardsInfo", poolCardsInfo);
};

const createOptimizerCard = (pool) => {
  return {
    id: pool.poolId,
    images: pool.lpIcon,
    pullPercent: pool.pullPercent,
    token0: {
      symbol: pool.token0.symbol,
      adress: pool.token0.adress,
    },
    token1: {
      symbol: pool.token1.symbol,
      adress: pool.token0.adress,
    },
  };
};
