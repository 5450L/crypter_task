import { ethers } from "ethers";

export const getTokenBalance = async (address, contract, decimals) => {
  const balance = await contract.balanceOf(address, { gasLimit: 5000000 });
  const parsedBalance = ethers.utils.formatUnits(balance, decimals);

  if (decimals === 0) return parseFloat(parsedBalance).toFixed(0);

  return parseFloat(parsedBalance).toFixed(4);
};
