import { Contract } from "ethers";

export const createTokenContract = async (tokenAdress, tokenAbi, signer) => {
  return await new Contract(tokenAdress, tokenAbi, signer);
};

export const getAllowance = async (
  userAddres,
  spenderAdress,
  tokenContract
) => {
  const allowance = await tokenContract.allowance(userAddres, spenderAdress, {
    gasLimit: 5000000,
  });
  return +allowance > 0;
};
