import { ethers } from "ethers";
import axios from "axios";
import { createTokenContract } from "./commonOperations";
import apePriceAbi from "@/utils/abi/price/apePriceAbi.json";

const chainCoinGeckoIds = {
  1: "ethereum",
  56: "binance-smart-chain",
  137: "polygon-pos",
  250: "fantom",
  42161: "arbitrum-one",
  43114: "avalanche",
};

const apiDomain = process.env.VUE_APP_COINGECKO_API_KEY
  ? "pro-api.coingecko.com"
  : "api.coingecko.com";

const config = {
  headers: {
    "X-Cg-Pro-Api-Key": process.env.VUE_APP_COINGECKO_API_KEY,
  },
};

export const getTokensRate = async (
  mainTokenContract,
  stakeTokenContract,
  stakeTokenAdress
) => {
  const sspellBalance = await mainTokenContract.balanceOf(stakeTokenAdress);
  const totalSupply = await stakeTokenContract.totalSupply();

  const parsedBalance = ethers.utils.formatEther(sspellBalance.toString());
  const parsedTotalSupply = ethers.utils.formatEther(totalSupply);

  const tokenRate = +parsedBalance / +parsedTotalSupply;

  return tokenRate.toFixed(4);
};

export const getTokenPriceByAddress = async (chainId, address) => {
  const chainCoinGeckoId = chainCoinGeckoIds[chainId];
  if (!chainCoinGeckoId) return false;

  const pricesResponse = await axios.get(
    `https://${apiDomain}/api/v3/simple/token_price/${chainCoinGeckoId}?contract_addresses=${address}&vs_currencies=usd`,
    config
  );

  let price = null;
  for (const property in pricesResponse.data) {
    price = pricesResponse.data[property]?.usd;
  }

  return parseFloat(+price).toFixed(4);
};

export const getApeRate = async (tokenContract) => {
  let rate = await tokenContract.convertToAssets("1000000000000000000");
  rate = +ethers.utils.formatUnits(rate);
  return rate.toFixed(4);
};

export const getApePrice = async (signer) => {
  const tokenContract = await createTokenContract(
    "0xd10abbc76679a20055e167bb80a24ac851b37056",
    apePriceAbi,
    signer
  );
  const tokenDecimals = await tokenContract.decimals();
  let price = await tokenContract.latestAnswer();
  price = +ethers.utils.formatUnits(price, tokenDecimals);
  return price;
};
