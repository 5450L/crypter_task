import { markRaw } from "vue";
import Web3Modal from "web3modal"
import { ethers } from "ethers";
import store from "@/store";

const providerOptions = {};

const web3Modal = new Web3Modal({
  providerOptions,
  cacheProvider: true,
  disableInjectedProvider: false,
  theme: "dark",
});

export const onConnect = async () => {
  try {
    const instance = await web3Modal.connect();
    await instance.enable();

    const provider = markRaw(new ethers.providers.Web3Provider(window.ethereum, "any"));
    const signer = provider.getSigner();
    const address = await signer.getAddress();
    const chainId = await signer.getChainId()
    const account = Array.isArray(address) ? address[0] : address;

    store.commit("SET_WALLET", {
      provider,
      signer,
      account,
      chainId
    });

    localStorage.setItem('metamaskIsConnected', true)
  } catch (error) {
    console.log("Connection error: ", error);
  }
};

export const resetApp = async () => {
  store.commit("SET_WALLET", {
    provider: null,
    signer: null,
    account: null,
    chainId:null
  });
  await web3Modal.clearCachedProvider();
  window.location.reload();
};


