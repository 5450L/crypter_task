import { Contract } from "ethers";
import { getAllowance } from "../commonOperations";
import cauldronsConfig from "@/utils/config/borrowPools/pools";
import oracleAbi from "@/utils/abi/oracleAbi.json";
import { getTokenBalance } from "../tokenBalance";
import { markRaw } from "vue";

import mimAbi from "@/utils/abi/MIM";
import mimIcon from "@/assets/images/tokens/MIM.png";

const borrowToken = {
  name: "MIM",
  icon: mimIcon,
  decimals: 18,
  abi: mimAbi,
  balance: 0,
};

export const getCauldronsData = async (account, signer, chainId) => {
  try {
    const cauldronsData = await Promise.all(
      cauldronsConfig[chainId].map(async (config) => {
        const collateralContract = new Contract(
          config.token.address,
          config.token.abi,
          signer
        );

        const borrowTokenContract = new Contract(
          config.borrowTokenAddress,
          borrowToken.abi,
          signer
        );

        const cauldronContract = new Contract(
          config.contract.address,
          config.contract.abi,
          signer
        );

        const oracleRate = await getOracleRate(cauldronContract, signer);

        borrowToken.balance = await getTokenBalance(
          account,
          borrowTokenContract,
          borrowToken.decimals
        );

        borrowToken.address = config.borrowTokenAddress;

        return {
          cauldronContract,
          isDegenBox: config.cauldronSettings.isDegenBox,
          collateralToken: {
            icon: config.icon,
            name: config.token.name,
            balance: await getTokenBalance(
              account,
              collateralContract,
              config.token.decimals
            ),
            decimals: config.token.decimals,
            contract: collateralContract,
            allowance: await getAllowance(
              account,
              borrowToken.address,
              collateralContract
            ),
          },
          borrowToken,
          rate: oracleRate,
        };
      })
    );
    return markRaw(cauldronsData);
  } catch {
    console.log("getBorrowInfo went wrong");
  }
};

const getOracleRate = async (cauldronContract, signer) => {
  const oracleAdress = await cauldronContract.oracle();
  const oracleData = await cauldronContract.oracleData();
  const oracleContract = new Contract(oracleAdress, oracleAbi, signer);

  const rate = await oracleContract.peekSpot(oracleData);
  return rate;
};
