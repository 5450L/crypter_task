import degenBoxAbi from "@/utils/abi/degenBoxAbi";
import bentoBoxAbi from "@/utils/abi/bentoBoxAbi";
import { Contract } from "ethers";

export const checkApprove = async (
  cauldronContract,
  bentoBoxContract,
  account
) => {
  const cauldronMasterContractAddress = await cauldronContract.masterContract();

  const approvement = await bentoBoxContract.masterContractApproved(
    cauldronMasterContractAddress,
    account
  );

  console.log(approvement);
  return approvement;
};

export const getBentoBoxContract = async (
  cauldronContract,
  isDegenBox,
  signer
) => {
  const bentoBoxAddress = await cauldronContract.bentoBox();

  const abiToUse = isDegenBox ? degenBoxAbi : bentoBoxAbi;

  return await new Contract(bentoBoxAddress, abiToUse, signer);
};
