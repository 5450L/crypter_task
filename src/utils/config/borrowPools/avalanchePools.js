import poolsAbi from "@/utils/abi/borrowPoolsAbi/index";
import tokensAbi from "@/utils/abi/tokensAbi/index";

import avaxIcon from "@/assets/images/tokens/AVAX.png";
import memoIcon from "@/assets/images/tokens/MEMO.png";
import mimIcon from "@/assets/images/tokens/MIM.png";

export default [
  {
    icon: avaxIcon,
    name: "WAVAX",
    contractChain: 43114,
    id: 1,
    stabilityFee: 12.5,
    interest: 1,
    ltv: 75,
    borrowFee: 0.5,

    contract: {
      name: "CauldronV2Flat",
      address: "0x3cfed0439ab822530b1ffbd19536d897ef30d2a2",
      abi: poolsAbi.CauldronV2Flat,
    },
    token: {
      name: "WAVAX",
      decimals: 18,
      address: "0xB31f66AA3C1e785363F0875A1B74E27b85FD66c7",
      abi: tokensAbi.WAVAX,
    },
    pairToken: {
      name: "MIM",
      icon: mimIcon,
      decimals: 18,
      address: "0x130966628846BFd36ff31a822705796e8cb8C18D",
      abi: tokensAbi.MIM,
    },
  },
  {
    icon: memoIcon,
    name: "wMEMO",
    contractChain: 43114,
    id: 2,
    stabilityFee: 12.5,
    interest: 5.5,
    ltv: 30,
    borrowFee: 1,

    contract: {
      name: "CauldronV2Multichain",
      address: "0x56984F04d2d04B2F63403f0EbeDD3487716bA49d",
      abi: poolsAbi.CauldronV2Multichain,
    },
    token: {
      name: "wMEMO",
      decimals: 18,
      address: "0x0da67235dD5787D67955420C84ca1cEcd4E5Bb3b",
      abi: tokensAbi.wMEMO,
    },
    pairToken: {
      name: "MIM",
      icon: mimIcon,
      decimals: 18,
      address: "0x130966628846BFd36ff31a822705796e8cb8C18D",
      abi: tokensAbi.MIM,
    },
  },
];
