import magicApeAbi from "@/utils/abi/magicApeAbi.json";
import spellAbi from "@/utils/abi/spellAbi.json";

import poolsAbi from "@/utils/abi/borrowPoolsAbi/index";

import magicApeIcon from "@/assets/images/stake/magicApeIcon.png";
import spellIcon from "@/assets/images/stake/spellIcon.png";

export default [
  {
    icon: spellIcon,
    name: "sSpell",
    contractChain: 1,
    id: 11,
    stabilityFee: 5,
    interest: 0.5,
    ltv: 85,
    borrowFee: 0.5,
    isSwappersActive: true,
    cauldronSettings: {
      isDegenBox: false,
      strategyLink: false,
      isDepreciated: true,
      acceptUseDefaultBalance: false,
      healthMultiplier: 1,
      hasAccountBorrowLimit: false,
      hasWithdrawableLimit: false,
      leverageMax: 15,
      localBorrowAmountLimit: 1000000,
      isCollateralClaimable: false,
      claimCrvReward: false,
    },
    contract: {
      name: "CauldronV2Flat",
      address: "0xC319EEa1e792577C319723b5e60a15dA3857E7da",
      abi: poolsAbi.CauldronV2Flat,
    },
    token: {
      name: "SPELL",
      decimals: 18,
      address: "0x090185f2135308BaD17527004364eBcC2D37e5F6",
      abi: spellAbi,
    },
    borrowTokenAddress: "0x99D8a9C45b2ecA8864373A26D1459e3Dff1e17F3",
  },
  {
    icon: magicApeIcon,
    name: "magicAPE",
    contractChain: 1,
    id: 39,
    stabilityFee: 7.5,
    interest: 18,
    ltv: 70,
    borrowFee: 0,
    isSwappersActive: true,
    is0xSwap: true,
    cauldronSettings: {
      isDegenBox: false,
      strategyLink: false,
      isDepreciated: false,
      acceptUseDefaultBalance: false,
      healthMultiplier: 1,
      hasAccountBorrowLimit: false,
      hasWithdrawableLimit: false,
      leverageMax: 15,
      localBorrowAmountLimit: false,
      isCollateralClaimable: false,
      claimCrvReward: false,
      isNew: true,
    },
    contract: {
      name: "CauldronV4",
      address: "0x692887E8877C6Dd31593cda44c382DB5b289B684",
      abi: poolsAbi.CauldronV4,
    },
    token: {
      name: "magicAPE",
      decimals: 18,
      address: "0xf35b31B941D94B249EaDED041DB1b05b7097fEb6",
      abi: magicApeAbi,
    },
    borrowTokenAddress: "0x99D8a9C45b2ecA8864373A26D1459e3Dff1e17F3",
  },
];
