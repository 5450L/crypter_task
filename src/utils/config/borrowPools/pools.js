import mainnetPools from "@/utils/config/borrowPools/mainnetPools";
import avalanchePools from "@/utils/config/borrowPools/avalanchePools";

export default {
  1: [...mainnetPools],
  43114: [...avalanchePools],
};
