import spellAbi from "@/utils/abi/spellAbi.json";
import sSpellAbi from "@/utils/abi/sSpellAbi.json";
import apeAbi from "@/utils/abi/apeAbi.json";
import magicApeAbi from "@/utils/abi/magicApeAbi.json";

import apePriceAbi from "@/utils/abi/price/apePriceAbi.json"


import spellIcon from "@/assets/images/stake/spellIcon.png";
import sSpellIcon from "@/assets/images/stake/sspellIcon.webp";
import apeIcon from "@/assets/images/stake/apeIcon.png";
import magicApeIcon from "@/assets/images/stake/magicApeIcon.png";

export const stakeConfig = {
  spell: {
    mainToken: {
      name: "SPELL",
      address: "0x090185f2135308BaD17527004364eBcC2D37e5F6",
      decimals: 18,
      abi: spellAbi,
      icon: spellIcon,
    },
    stakeToken: {
      name: "sSPELL",
      address: "0x26FA3fFFB6EfE8c1E69103aCb4044C26B9A106a9",
      decimals: 18,
      abi: sSpellAbi,
      icon: sSpellIcon,
    },
  },
  ape: {
    mainToken: {
      name: "APE",
      address: "0x4d224452801ACEd8B2F0aebE155379bb5D594381",
      decimals: 18,
      priceAbi: apePriceAbi,
      abi: apeAbi,
      icon: apeIcon,
    },
    stakeToken: {
      name: "magicAPE",
      address: "0xf35b31B941D94B249EaDED041DB1b05b7097fEb6",
      decimals: 18,
      abi: magicApeAbi,
      icon: magicApeIcon,
    },
  },
};
