import abiERC20 from "@/utils/abi/abiERC20";
import SwapAbi from "@/utils/abi/swapAbi.json";

import gwContractAbi from "@/utils/abi/pools/gala_weth/gwContractAbi.json";
import gwPoolAbi from "@/utils/abi/pools/gala_weth/gwPoolAbi.json";
import fwContractAbi from "@/utils/abi/pools/fxs_weth/fwContractAbi.json";
import fwPoolAbi from "@/utils/abi/pools/fxs_weth/fwPoolAbi.json";

import galaIcon from "@/assets/images/tokens/GALA.png";
import wethIcon from "@/assets/images/tokens/WETH.png";
import fxsIcon from "@/assets/images/tokens/FXS.png";

import gwPoolIcon from "@/assets/images/pools/GALA_WETH.png";
import fwPoolIcon from "@/assets/images/pools/FXS_WETH.png";

export default [
  {
    chainId: 1,
    isDeprecated: false,
    poolId: 1,
    pullPercent: 0.3,
    name: "GALA/WETH",
    lpDecimals: 18,
    lpIcon: gwPoolIcon,
    contract: {
      adress: "0x4703e6C963E72F6132385E523B4a8Aa0236d5c9D",
      abi: gwContractAbi,
    },
    poolContract: {
      adress: "0xf8a95b2409C27678A6d18D950c5d913D5C38aB03",
      abi: gwPoolAbi,
    },
    token0: {
      symbol: "GALA",
      icon: galaIcon,
      canBeDefault: false,
      decimals: 8,
      adress: "0x15D4c048F83bd7e37d49eA4C83a07267Ec4203dA",
      abi: abiERC20,
    },
    token1: {
      symbol: "WETH",
      icon: wethIcon,
      canBeDefault: true,
      decimals: 18,
      adress: "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
      abi: abiERC20,
    },
    swap: {
      address: "0x34E9E83608790e91eBD14f0F84Da35BB21CBd3F5",
      abi: SwapAbi,
    },
  },
  {
    chainId: 1,
    isDeprecated: false,
    poolId: 2,
    pullPercent: 1,
    name: "FXS/WETH",
    lpDecimals: 18,
    lpIcon: fwPoolIcon,
    contract: {
      adress: "0xa6B993Bd90fD68e90E0F87BA58B4F783dFbFfb69",
      abi: fwContractAbi,
    },
    poolContract: {
      adress: "0xCD8286b48936cDAC20518247dBD310ab681A9fBf",
      abi: fwPoolAbi,
    },
    token0: {
      symbol: "FXS",
      icon: fxsIcon,
      canBeDefault: false,
      decimals: 18,
      adress: "0x3432B6A60D23Ca0dFCa7761B7ab56459D9C964D0",
      abi: abiERC20,
    },
    token1: {
      symbol: "WETH",
      icon: wethIcon,
      canBeDefault: true,
      decimals: 18,
      adress: "0xC02aaA39b223FE8D0A0e5C4F27eAD9083C756Cc2",
      abi: abiERC20,
    },
    swap: {
      address: "0x34E9E83608790e91eBD14f0F84Da35BB21CBd3F5",
      abi: SwapAbi,
    },
  },
];
