import { createStore } from "vuex";

import fetching from "./modules/fetching";
import categories from "./modules/categories";
import brands from "./modules/brands";
import filters from "./modules/filters";
import wallet from "./modules/wallet";
import countDown from "./modules/countDown";
import optimizerPools from "./modules/optimizerPools";

export default createStore({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    fetching,
    categories,
    brands,
    filters,
    wallet,
    countDown,
    optimizerPools,
  },
});
