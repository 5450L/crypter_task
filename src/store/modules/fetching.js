import { productsApi } from "@/api/api";
import { sortBrands } from "@/helpers/filtration/brandsFiltration";

export default {
  state: {
    products: [],
    allCategories: ["all"],
    isLoading: true,
  },
  getters: {
    getProducts: (state) => state.products,
    getAllCategories: (state) => state.allCategories,
    getIsLoading: (state) => state.isLoading,
    getProductById: (state) => (id) => {
      return state.products.find((product) => product.id == id);
    },
  },
  mutations: {
    setProducts(state, products) {
      state.products = products;
    },
    setCategories(state, categories) {
      state.allCategories = ["all", ...categories];
    },
    setIsLoading(state, payload) {
      state.isLoading = payload;
    },
  },
  actions: {
    async fetchProductsInfo(context) {
      context.commit("setIsLoading", true);

      const allProducts = await productsApi.downloadAllProducts();
      const allCategories = await productsApi.downloadCategoriesList();

      if (context.state.products.length <= 0) {
        context.commit("setProducts", allProducts);
        context.commit("setCategories", allCategories);
      }
      sortBrands();

      context.commit("setIsLoading", false);
    },
  },
};
