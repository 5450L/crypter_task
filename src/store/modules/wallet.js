export default {
    state: {
        provider: null,
        signer: null,
        account: null,
        chainId: null
    },
    mutations: {
        SET_WALLET(state, { provider, signer, account, chainId }) {
            state.provider = provider;
            state.signer = signer;
            state.account = account;
            state.chainId = chainId
        }
    },
    getters: {
        getProvider: (state) => state.provider,
        getSigner: (state) => state.signer,
        getAccount: (state) => state.account,
        getChainId: (state) => state.chainId
    },
};