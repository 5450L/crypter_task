export default ({
    state: {
        endOfDelay: null
    },
    getters: {
        getEndOfDelay: state => state.endOfDelay
    },
    mutations: {
        setEndOfDelay(state, payload) {
            state.endOfDelay = payload
        }
    },
    actions: {
        initCountDown(context) {
            localStorage.removeItem('END_OF_DELAY')

            const currentDate = new Date();
            const endOfDelay = new Date(currentDate.getTime() + 86400000)
            localStorage.setItem('END_OF_DELAY', endOfDelay.getTime())
            context.commit('setEndOfDelay', endOfDelay)
        }
    },
})