import { filterProducts } from "@/helpers/filtration/productsFiltration";
import { sortBrands } from "@/helpers/filtration/brandsFiltration";


export default ({
    state: {
        chosenCategories: ['all']
    },
    getters: {
        getChosenCategories: state => state.chosenCategories
    },
    mutations: {
        setChosenCategory(state, chosenCategory) {
            if (chosenCategory === 'all' || state.chosenCategories.includes('all')) {
                state.chosenCategories = [chosenCategory]
            } else if (state.chosenCategories.includes(chosenCategory)) {
                state.chosenCategories = state.chosenCategories.filter(category => category !== chosenCategory)
                if (!state.chosenCategories.length) state.chosenCategories = ['all']
            } else { state.chosenCategories.push(chosenCategory) }
        }
    },
    actions: {
        addChosenCategory(context, payload) {
            context.commit('setChosenCategory', payload)
            sortBrands();
            filterProducts()
        }
    },
})