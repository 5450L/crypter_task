export default {
  state: {
    optimizerPoolsInfo: [],
    optimizerPoolCardsInfo: [],
  },
  getters: {
    getOptimizerPoolsInfo: (state) => state.optimizerPoolsInfo,
    getPoolById: (state) => (id) => {
      return state.optimizerPoolsInfo.find((pool) => pool.poolId == id);
    },
    getOptimizerPoolCardsInfo: (state) => state.optimizerPoolCardsInfo,
  },
  mutations: {
    setOptimizerPoolsInfo(state, payload) {
      state.optimizerPoolsInfo = payload;
    },
    setOptimizerPoolCardsInfo(state, payload) {
      state.optimizerPoolCardsInfo = payload;
    },
  },
  actions: {},
};
