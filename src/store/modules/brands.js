export default ({
    state: {
        sortedBrands: ['All']
    },
    getters: {
        getSortedBrands: state => state.sortedBrands
    },
    mutations: {
        setSortedBrands(state, sortedBrands) {
            state.sortedBrands = sortedBrands
        }
    },
    actions: {
        fetchSortedBrands(context, sortedBrands) {
            context.commit('setSortedBrands', sortedBrands)
        }
    },
})