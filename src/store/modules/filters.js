export default {
  state: {
    filteredProducts: [],
    attachedFilters: {
      price: "None",
      priceRange: null,
      rating: "None",
      brand: "All",
    },
  },
  getters: {
    getAttachedFilters: (state) => state.attachedFilters,
    getFilteredProducts: (state) => state.filteredProducts,
  },
  mutations: {
    setFilteredProducts(state, payload) {
      state.filteredProducts = payload;
    },
    setPrice(state, payload) {
      state.attachedFilters.price = payload;
    },
    setPriceRange(state, payload) {
      state.attachedFilters.priceRange = payload;
    },
    setRating(state, payload) {
      state.attachedFilters.rating = payload;
    },
    setBrand(state, payload) {
      state.attachedFilters.brand = payload;
    },
  },
  actions: {
    fetchFilteredProducts(context, filteredProducts) {
      context.commit("setFilteredProducts", filteredProducts);
    },
    changeFilter(context, { title, option }) {
      switch (title) {
        case "price":
          context.commit("setPrice", option);
          break;
        case "rating":
          context.commit("setRating", option);
          break;
        case "brand":
          context.commit("setBrand", option);
          break;
        default:
          break;
      }
    },
  },
};
