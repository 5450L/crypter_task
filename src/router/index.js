import { createRouter, createWebHistory } from "vue-router";
import MainView from "../views/MainView.vue";

const routes = [
  {
    path: "/",
    name: "main",
    component: MainView,
  },
  {
    path: "/product/:id",
    name: "product",
    component: () => import("@/views/ProductView.vue"),
  },
  {
    path: "/dashboard",
    name: "dashboard",
    component: () => import("@/views/DashBoard.vue"),
  },
  {
    path: "/SPELL",
    name: "spell",
    component: () => import("@/views/staking/StakeSPELL.vue"),
  },
  {
    path: "/APE",
    name: "ape",
    component: () => import("@/views/staking/StakeAPE.vue"),
  },
  {
    path: "/w3optimizer",
    name: "w3optimizer",
    component: () => import("@/views/pools/W3Optimizer/PoolsW3.vue"),
  },
  {
    path: "/w3optimizer/:id",
    name: "poolW3",
    component: () => import("@/views/pools/W3Optimizer/PoolW3.vue"),
  },
  {
    path: "/abracadabra",
    name: "abracadabra",
    component: () => import("@/views/abracadabra/Abracadabra.vue"),
    children: [
      {
        path: "borrow",
        name: "borrow",
        component: () =>
          import("@/views/abracadabra/features/borrow/BorrowPage.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
