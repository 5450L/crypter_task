import { defineAsyncComponent } from "vue";
import { mapGetters } from "vuex";

export default {
  computed: {
    ...mapGetters({
      provider: "getProvider",
      signer: "getSigner",
      account: "getAccount",
      chainId: "getChainId",
    }),
  },
  components: {
    Loader: defineAsyncComponent(() =>
      import("@/components/common/Loader.vue")
    ),
    CommonButton: defineAsyncComponent(() =>
      import("@/components/common/CommonButton.vue")
    ),
    CommonDropdown: defineAsyncComponent(() =>
      import("@/components/common/CommonDropdown.vue")
    ),
  },
};
