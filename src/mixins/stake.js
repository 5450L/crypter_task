import commonMixin from "@/mixins/common";
export default {
  mixins: [commonMixin],
  data() {
    return {
      mainToken: null,
      stakeToken: null,
      tokenRate: null,
      isStake: true,
      fromAmount: null,
      toAmount: null,
      updateInterval: null,
    };
  },
  computed: {
    buttonTitle() {
      if (this.isStake) {
        return this.stakeToken.allowance ? "Stake" : "Approve";
      } else if (this.endOfDelay) {
        return `${this.countDown.hours} : ${this.countDown.minutes} : ${this.countDown.seconds}`;
      }
      return "Unstake";
    },
    fromToken() {
      if (this.isStake) return this.mainToken;
      return this.stakeToken;
    },
    toToken() {
      if (this.isStake) return this.stakeToken;
      return this.mainToken;
    },
  },
  watch: {
    async chainId() {
      await this.createLocal();
    },
    fromAmount(newValue) {
      if (newValue < 0) {
        this.fromAmount = null;
        alert("You can not pass negative amount !");
      }
    },
  },
  methods: {
    toggleButton() {
      this.isStake = !this.isStake;
      this.countAmounts();
    },
    countAmounts() {
      if (this.isStake) {
        this.toAmount = this.fromAmount / this.tokenRate;
      } else {
        this.toAmount = this.fromAmount * this.tokenRate;
      }
    },
    actionHandler() {
      switch (this.buttonTitle) {
        case "Approve":
          this.approve();
          break;
        case "Stake":
          this.stake();
          break;
        case "Unstake":
          this.unstake();
          break;
      }
    },
  },
};
