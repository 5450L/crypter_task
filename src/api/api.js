import { filterProducts } from "@/helpers/filtration/productsFiltration";
export const productsApi = {
  async downloadAllProducts() {
    return await fetch("https://dummyjson.com/products?limit=0")
      .then((res) => res.json())
      .then((productsData) => {
        let products = [];
        productsData.products.map((product) => {
          products.push({ ...product, bids: [], wish: false });
        });

        return filterProducts(products);
      });
  },
  async downloadCategoriesList() {
    return await fetch("https://dummyjson.com/products/categories").then(
      (res) => res.json()
    );
  },
};
