import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import clickOutside from "./directives/clickOutside";
import Popper from "vue3-popper";

const app = createApp(App);
app.use(store);
app.use(router);
app.component("PopperComponent", Popper);
app.directive("click-outside", clickOutside);

app.mount("#app");
